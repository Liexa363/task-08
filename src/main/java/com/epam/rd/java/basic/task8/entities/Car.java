package com.epam.rd.java.basic.task8.entities;

public class Car {
    private String name;

    private String origin;

    private String type;

    private int year;

    private Specifications specifications;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Specifications getSpecifications() {
        return specifications;
    }

    public void setSpecifications(Specifications specifications) {
        this.specifications = specifications;
    }

    @Override
    public String toString() {
        return "Car" +
                "\n\tname=" + name +
                "\n\torigin=" + origin +
                "\n\ttype=" + type +
                "\n\tyear=" + year +
                "\n\tspecifications" + specifications + "\n";
    }
}

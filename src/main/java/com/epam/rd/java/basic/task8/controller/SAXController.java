package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entities.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;

	private String currentElement;

	private Cars cars;

	private Car car;

	private Specifications specifications;

	private Engine engine;

	private Dimensions dimensions;

	private FuelConsumption fuelConsumption;




	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public final void parse( boolean validate)
			throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		if (validate) {
			factory.setFeature(Constants.TURN_VALIDATION_ON, true);
			factory.setFeature(Constants.TURN_SCHEMA_VALIDATION_ON, true);
		}

		SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName, this);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		currentElement = localName;
		if(XML.CARS.equalsTo(currentElement)){
			cars = new Cars();
			return;
		}
		if(XML.CAR.equalsTo(currentElement)){
			car = new Car();
			return;
		}
		if(XML.SPECIFICATIONS.equalsTo(currentElement)){
			specifications = new Specifications();
			return;
		}
		if(XML.ENGINE.equalsTo(currentElement)){
			engine = new Engine();
			return;
		}
		if(XML.DIMENSIONS.equalsTo(currentElement)){
			dimensions = new Dimensions();
			return;
		}
		if(XML.FUEL_CONSUMPTIONS.equalsTo(currentElement)){
			fuelConsumption = new FuelConsumption();
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String elementText = new String(ch, start, length).trim();

		if (elementText.isEmpty()) {
			return;
		}
		if (XML.NAME.equalsTo(currentElement)) {
			car.setName(elementText);
			return;
		}
		if (XML.ORIGIN.equalsTo(currentElement)) {
			car.setOrigin(elementText);
			return;
		}
		if (XML.TYPE.equalsTo(currentElement)) {
			car.setType(elementText);
			return;
		}
		if (XML.YEAR.equalsTo(currentElement)) {
			car.setYear(Integer.parseInt(elementText));
			return;
		}
		if (XML.PRICE.equalsTo(currentElement)) {
			specifications.setPrice(Integer.parseInt(elementText));
			return;
		}
		if (XML.POWER.equalsTo(currentElement)) {
			engine.setPower(Integer.parseInt(elementText));
			return;
		}
		if (XML.TORQUE.equalsTo(currentElement)) {
			engine.setTorque(Integer.parseInt(elementText));
			return;
		}
		if (XML.LENGTH.equalsTo(currentElement)) {
			dimensions.setLength(Integer.parseInt(elementText));
			return;
		}
		if (XML.WIDTH.equalsTo(currentElement)) {
			dimensions.setWidth(Integer.parseInt(elementText));
			return;
		}
		if (XML.HEIGHT.equalsTo(currentElement)) {
			dimensions.setHeight(Integer.parseInt(elementText));
			return;
		}
		if (XML.PASSENGER_VOLUME.equalsTo(currentElement)) {
			dimensions.setPassengerVolume(Integer.parseInt(elementText));
			return;
		}
		if (XML.CARGO_VOLUME.equalsTo(currentElement)) {
			dimensions.setCargoVolume(Integer.parseInt(elementText));
			return;
		}
		if (XML.GENERAL.equalsTo(currentElement)) {
			fuelConsumption.setGeneral(Double.parseDouble(elementText));
			return;
		}
		if (XML.CITY.equalsTo(currentElement)) {
			fuelConsumption.setCity(Double.parseDouble(elementText));
			return;
		}
		if (XML.HIGHWAY.equalsTo(currentElement)) {
			fuelConsumption.setHighway(Double.parseDouble(elementText));
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(XML.CAR.equalsTo(localName)){
			cars.addCar(car);
			car = null;
			return;
		}
		if(XML.SPECIFICATIONS.equalsTo(localName)){
			car.setSpecifications(specifications);
			specifications = null;
			return;
		}
		if(XML.ENGINE.equalsTo(localName)){
			specifications.setEngine(engine);
			engine = null;
			return;
		}
		if(XML.DIMENSIONS.equalsTo(localName)){
			specifications.setDimensions(dimensions);
			dimensions = null;
			return;
		}
		if(XML.FUEL_CONSUMPTIONS.equalsTo(localName)){
			specifications.setFuelConsumptions(fuelConsumption);
			fuelConsumption = null;
		}
	}

	public Cars getCars() {
		return cars;
	}

	public void setCars(Cars cars) {
		this.cars = cars;
	}
}
package com.epam.rd.java.basic.task8.entities;

public class FuelConsumption {
    private double general;
    private double city;
    private double highway;

    public double getGeneral() {
        return general;
    }

    public void setGeneral(double general) {
        this.general = general;
    }

    public double getCity() {
        return city;
    }

    public void setCity(double city) {
        this.city = city;
    }

    public double getHighway() {
        return highway;
    }

    public void setHighway(double highway) {
        this.highway = highway;
    }

    @Override
    public String toString() {
        return "FuelConsumptions{" +
                "general=" + general +
                ", city=" + city +
                ", highway=" + highway +
                '}';
    }
}

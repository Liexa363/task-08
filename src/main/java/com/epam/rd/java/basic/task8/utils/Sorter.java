package com.epam.rd.java.basic.task8.utils;

import com.epam.rd.java.basic.task8.entities.Car;
import com.epam.rd.java.basic.task8.entities.Cars;

import java.util.Comparator;

/**
 * public class {@code Sorter} is designed to perform sorting parsed objects from <b>xml</b>
 */
public final class Sorter {

    /**
     * Comparator - {@code SORT_CARS_BY_NAME} is designed to perform sorting cars by name in {@link Sorter#sortCarsByName} method
     */
    public static final Comparator<Car>
    SORT_CARS_BY_NAME = (first, second) -> {
        String carName1 = first.getName();
        String carName2 = second.getName();
        return carName1.compareTo(carName2);
    };

    /**
     * Comparator - {@code SORT_CARS_BY_PRICE} is designed to perform sorting cars by name in {@link Sorter#sortCarsByPrice} method
     */
    public static final Comparator<Car>
    SORT_CARS_BY_PRICE = (first, second) -> {
        Integer carPrice1 = first.getSpecifications().getPrice();
        Integer carPrice2 = second.getSpecifications().getPrice();
        return carPrice1.compareTo(carPrice2);
    };

    /**
     * Comparator - {@code SORT_CARS_BY_ENGINE_POWER} is designed to perform sorting cars by name in {@link Sorter#sortCarsByEnginePower} method
     */
    public static final Comparator<Car>
    SORT_CARS_BY_ENGINE_POWER = (first, second) -> {
        Integer enginePower1 = first.getSpecifications().getEngine().getPower();
        Integer enginePower2 = second.getSpecifications().getEngine().getPower();
        return enginePower1.compareTo(enginePower2);
    };

    public static void sortCarsByName(Cars cars) {
        cars.getCars().sort(SORT_CARS_BY_NAME);
    }

    public static void sortCarsByPrice(Cars cars) {
        cars.getCars().sort(SORT_CARS_BY_PRICE);
    }

    public static void sortCarsByEnginePower(Cars cars) {
        cars.getCars().sort(SORT_CARS_BY_ENGINE_POWER);
    }

    private Sorter() {

    }


}

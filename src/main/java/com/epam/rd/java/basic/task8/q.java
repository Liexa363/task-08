package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;
import com.epam.rd.java.basic.task8.utils.Sorter;
import com.epam.rd.java.basic.task8.entities.Cars;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public class q {

    public static void main(String[] args) {

        DOMController domController = new DOMController(Constants.VALID_XML_FILE);
        SAXController saxController = new SAXController(Constants.VALID_XML_FILE);
        STAXController staxController = new STAXController(Constants.VALID_XML_FILE);
        try {
            domController.parse(true);
            Cars cars = domController.getCars();
            Sorter.sortCarsByPrice(cars);
            DOMController.saveToXML(cars,"output.dom.xml");

            saxController.parse(true);
            DOMController.saveToXML(saxController.getCars(),"output.sax.xml");

            staxController.parse();
            DOMController.saveToXML(staxController.getCars(),"output.stax.xml");
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }
}

package com.epam.rd.java.basic.task8.entities;

public class Dimensions {

    private int length;
    private int width;
    private int height;
    private int passengerVolume;
    private int cargoVolume;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getPassengerVolume() {
        return passengerVolume;
    }

    public void setPassengerVolume(int passengerVolume) {
        this.passengerVolume = passengerVolume;
    }

    public int getCargoVolume() {
        return cargoVolume;
    }

    public void setCargoVolume(int cargoVolume) {
        this.cargoVolume = cargoVolume;
    }

    @Override
    public String toString() {
        return "Dimensions{" +
                "length=" + length +
                ", width=" + width +
                ", height=" + height +
                ", passengerVolume=" + passengerVolume +
                ", cargoVolume=" + cargoVolume +
                '}';
    }
}

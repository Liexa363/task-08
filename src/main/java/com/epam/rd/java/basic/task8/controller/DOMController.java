package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entities.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	private Cars cars;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	// PLACE YOUR CODE HERE

	public final void parse(boolean validate) throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);

		if(validate){
			dbf.setFeature(Constants.TURN_VALIDATION_ON,true);
			dbf.setFeature(Constants.TURN_SCHEMA_VALIDATION_ON,true);
		}

		DocumentBuilder db = dbf.newDocumentBuilder();

		Document document = db.parse(xmlFileName);

		Element root = document.getDocumentElement();

		cars = new Cars();

		NodeList carNodes = root.getElementsByTagName(XML.CAR.value());

		for (int i = 0; i < carNodes.getLength(); i++) {
			Car car = getCar(carNodes.item(i));
			cars.addCar(car);
		}

	}

	private Car getCar(Node node) {
		Car car = new Car();
		Element element = (Element) node;

		NodeList dNode = element.getElementsByTagName(XML.NAME.value());
		car.setName(dNode.item(0).getTextContent());

		dNode = element.getElementsByTagName(XML.ORIGIN.value());
		car.setOrigin(dNode.item(0).getTextContent());

		dNode = element.getElementsByTagName(XML.TYPE.value());
		car.setType(dNode.item(0).getTextContent());

		dNode = element.getElementsByTagName(XML.YEAR.value());
		car.setYear(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XML.SPECIFICATIONS.value());
		car.setSpecifications(getSpecifications(dNode.item(0)));

		System.out.println(car);
		return car;
	}

	private Specifications getSpecifications(Node node) {
		Specifications specifications = new Specifications();

		Element element = (Element) node;

		NodeList dNode = element.getElementsByTagName(XML.PRICE.value());
		specifications.setPrice(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XML.ENGINE.value());
		specifications.setEngine(getEngine(dNode.item(0)));

		dNode = element.getElementsByTagName(XML.DIMENSIONS.value());
		specifications.setDimensions(getDimensions(dNode.item(0)));

		dNode = element.getElementsByTagName(XML.FUEL_CONSUMPTIONS.value());
		specifications.setFuelConsumptions(getFuelConsumptions(dNode.item(0)));


		return specifications;
	}

	private Engine getEngine(Node node) {
		Engine engine = new Engine();
		Element element = (Element) node;

		NodeList dNode = element.getElementsByTagName(XML.POWER.value());
		engine.setPower(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XML.TORQUE.value());
		engine.setTorque(Integer.parseInt(dNode.item(0).getTextContent()));

		return engine;

	}

	private Dimensions getDimensions(Node node) {
		Dimensions dimensions = new Dimensions();
		Element element = (Element) node;

		NodeList dNode = element.getElementsByTagName(XML.LENGTH.value());
		dimensions.setLength(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XML.WIDTH.value());
		dimensions.setWidth(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XML.HEIGHT.value());
		dimensions.setHeight(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XML.PASSENGER_VOLUME.value());
		dimensions.setPassengerVolume(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XML.CARGO_VOLUME.value());
		dimensions.setCargoVolume(Integer.parseInt(dNode.item(0).getTextContent()));

		return dimensions;
	}

	private FuelConsumption getFuelConsumptions(Node node) {
	FuelConsumption fuelConsumptions = new FuelConsumption();
	Element element = (Element) node;

	NodeList dNode = element.getElementsByTagName(XML.GENERAL.value());
	fuelConsumptions.setGeneral(Double.parseDouble(dNode.item(0).getTextContent()));

	dNode = element.getElementsByTagName(XML.CITY.value());
	fuelConsumptions.setCity(Double.parseDouble(dNode.item(0).getTextContent()));

	dNode = element.getElementsByTagName(XML.HIGHWAY.value());
	fuelConsumptions.setHighway(Double.parseDouble(dNode.item(0).getTextContent()));

	return fuelConsumptions;
	}

	public static Document getDocument(Cars cars) throws ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		Element root = document.createElement(XML.CARS.value());
		root.setAttribute("xmlns",Constants.XMLNS);
		root.setAttribute("xmlns:xsi",Constants.XMLNS_XSI);
		root.setAttribute("xsi:schemaLocation",Constants.XSI_SCHEMA_LOCATION);
		document.appendChild(root);

		for(Car car : cars.getCars()){

			Element carElement = document.createElement(XML.CAR.value());
			root.appendChild(carElement);

			Element nameElement = document.createElement(XML.NAME.value());
			nameElement.setTextContent(car.getName());
			carElement.appendChild(nameElement);

			Element originElement = document.createElement(XML.ORIGIN.value());
			originElement.setTextContent(car.getOrigin());
			carElement.appendChild(originElement);

			Element typeElement = document.createElement(XML.TYPE.value());
			typeElement.setTextContent(car.getType());
			carElement.appendChild(typeElement);

			Element yearElement = document.createElement(XML.YEAR.value());
			yearElement.setTextContent(String.valueOf(car.getYear()));
			carElement.appendChild(yearElement);

			//--------------------------------------------------------------------------------
			Element specificationsElement = document.createElement(XML.SPECIFICATIONS.value());
			carElement.appendChild(specificationsElement);

			final String carPrice = String.valueOf(car.getSpecifications().getPrice());
			Element priceElement = document.createElement(XML.PRICE.value());
			priceElement.setTextContent(carPrice);
			specificationsElement.appendChild(priceElement);

			Element engineElement = document.createElement(XML.ENGINE.value());
			specificationsElement.appendChild(engineElement);

			final String enginePower = String.valueOf(car.getSpecifications().getEngine().getPower());
			final String engineTorque = String.valueOf(car.getSpecifications().getEngine().getTorque());

			Element powerElement = document.createElement(XML.POWER.value());
			powerElement.setTextContent(enginePower);
			engineElement.appendChild(powerElement);

			Element torqueElement = document.createElement(XML.TORQUE.value());
			torqueElement.setTextContent(engineTorque);
			engineElement.appendChild(torqueElement);

			Element dimensionsElement = document.createElement(XML.DIMENSIONS.value());
			specificationsElement.appendChild(dimensionsElement);

			final String carLength = String.valueOf(car.getSpecifications().getDimensions().getLength());
			final String carWidth = String.valueOf(car.getSpecifications().getDimensions().getWidth());
			final String carHeight = String.valueOf(car.getSpecifications().getDimensions().getHeight());
			final String carPassengerVolume = String.valueOf(car.getSpecifications().getDimensions().getPassengerVolume());
			final String carCargoVolume = String.valueOf(car.getSpecifications().getDimensions().getCargoVolume());

			Element lengthElement = document.createElement(XML.LENGTH.value());
			lengthElement.setTextContent(carLength);
			dimensionsElement.appendChild(lengthElement);

			Element widthElement = document.createElement(XML.WIDTH.value());
			widthElement.setTextContent(carWidth);
			dimensionsElement.appendChild(widthElement);

			Element heightElement = document.createElement(XML.HEIGHT.value());
			heightElement.setTextContent(carHeight);
			dimensionsElement.appendChild(heightElement);

			Element passengerVolumeElement = document.createElement(XML.PASSENGER_VOLUME.value());
			passengerVolumeElement.setTextContent(carPassengerVolume);
			dimensionsElement.appendChild(passengerVolumeElement);

			Element cargoVolumeElement = document.createElement(XML.CARGO_VOLUME.value());
			cargoVolumeElement.setTextContent(carCargoVolume);
			dimensionsElement.appendChild(cargoVolumeElement);

			Element fuelConsumptionsElement = document.createElement(XML.FUEL_CONSUMPTIONS.value());
			specificationsElement.appendChild(fuelConsumptionsElement);

			final String general = String.valueOf(car.getSpecifications().getFuelConsumptions().getGeneral());
			final String city = String.valueOf(car.getSpecifications().getFuelConsumptions().getCity());
			final String highway = String.valueOf(car.getSpecifications().getFuelConsumptions().getHighway());

			Element generalElement = document.createElement(XML.GENERAL.value());
			generalElement.setTextContent(general);
			fuelConsumptionsElement.appendChild(generalElement);

			Element cityElement = document.createElement(XML.CITY.value());
			cityElement.setTextContent(city);
			fuelConsumptionsElement.appendChild(cityElement);

			Element highwayElement = document.createElement(XML.HIGHWAY.value());
			highwayElement.setTextContent(highway);
			fuelConsumptionsElement.appendChild(highwayElement);
		}

		return document;
	}

	public static void saveToXML(Cars cars,String xmlFileName) throws ParserConfigurationException, TransformerException {
		saveToXML(getDocument(cars),xmlFileName);
	}

	public static void saveToXML(Document document,String xmlFileName) throws TransformerException {
		StreamResult result = new StreamResult(new File(xmlFileName));
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT,"yes");
		transformer.transform(new DOMSource(document),result);
	}

	public Cars getCars() {
		return cars;
	}
}

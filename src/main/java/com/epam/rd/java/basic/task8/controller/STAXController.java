package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entities.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;

	private Cars cars;

	private Car car;

	private Specifications specifications;

	private Engine engine;

	private Dimensions dimensions;

	private FuelConsumption fuelConsumption;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public void parse() throws ParserConfigurationException,
			SAXException, IOException, XMLStreamException {

		String currentElement = null;

		XMLInputFactory factory = XMLInputFactory.newInstance();

		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();

			if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
				continue;
			}

			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();
				if(XML.CARS.equalsTo(currentElement)){
					cars = new Cars();
				}
				if(XML.CAR.equalsTo(currentElement)){
					car = new Car();
				}
				if(XML.SPECIFICATIONS.equalsTo(currentElement)){
					specifications = new Specifications();
				}
				if(XML.ENGINE.equalsTo(currentElement)){
					engine = new Engine();
				}
				if(XML.DIMENSIONS.equalsTo(currentElement)){
					dimensions = new Dimensions();
				}
				if(XML.FUEL_CONSUMPTIONS.equalsTo(currentElement)){
					fuelConsumption = new FuelConsumption();
				}
			}

			if (event.isCharacters()) {
				Characters characters = event.asCharacters();
				String elementText = characters.getData();
				if (XML.NAME.equalsTo(currentElement)) {
					car.setName(elementText);
				}
				if (XML.ORIGIN.equalsTo(currentElement)) {
					car.setOrigin(elementText);
				}
				if (XML.TYPE.equalsTo(currentElement)) {
					car.setType(elementText);
				}
				if (XML.YEAR.equalsTo(currentElement)) {
					car.setYear(Integer.parseInt(elementText));
				}
				if (XML.PRICE.equalsTo(currentElement)) {
					specifications.setPrice(Integer.parseInt(elementText));
				}
				if (XML.POWER.equalsTo(currentElement)) {
					engine.setPower(Integer.parseInt(elementText));
				}
				if (XML.TORQUE.equalsTo(currentElement)) {
					engine.setTorque(Integer.parseInt(elementText));
				}
				if (XML.LENGTH.equalsTo(currentElement)) {
					dimensions.setLength(Integer.parseInt(elementText));
				}
				if (XML.WIDTH.equalsTo(currentElement)) {
					dimensions.setWidth(Integer.parseInt(elementText));
				}
				if (XML.HEIGHT.equalsTo(currentElement)) {
					dimensions.setHeight(Integer.parseInt(elementText));
				}
				if (XML.PASSENGER_VOLUME.equalsTo(currentElement)) {
					dimensions.setPassengerVolume(Integer.parseInt(elementText));
				}
				if (XML.CARGO_VOLUME.equalsTo(currentElement)) {
					dimensions.setCargoVolume(Integer.parseInt(elementText));
				}
				if (XML.GENERAL.equalsTo(currentElement)) {
					fuelConsumption.setGeneral(Double.parseDouble(elementText));
				}
				if (XML.CITY.equalsTo(currentElement)) {
					fuelConsumption.setCity(Double.parseDouble(elementText));
				}
				if (XML.HIGHWAY.equalsTo(currentElement)) {
					fuelConsumption.setHighway(Double.parseDouble(elementText));
				}
			}

			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				String localName = endElement.getName().getLocalPart();

				if(XML.CAR.equalsTo(localName)){
					cars.addCar(car);
					car = null;
				}
				if(XML.SPECIFICATIONS.equalsTo(localName)){
					car.setSpecifications(specifications);
					specifications = null;
				}
				if(XML.ENGINE.equalsTo(localName)){
					specifications.setEngine(engine);
					engine = null;
				}
				if(XML.DIMENSIONS.equalsTo(localName)){
					specifications.setDimensions(dimensions);
					dimensions = null;
				}
				if(XML.FUEL_CONSUMPTIONS.equalsTo(localName)){
					specifications.setFuelConsumptions(fuelConsumption);
					fuelConsumption = null;
				}
			}
		}
		reader.close();
	}

	public Cars getCars() {
		return cars;
	}
}
package com.epam.rd.java.basic.task8.entities;


import java.util.ArrayList;
import java.util.List;

public class Cars {

    private List<Car> cars;

    public Cars(){
        cars = new ArrayList<Car>();
    }

    public final List<Car> getCars(){
        return this.cars;
    }

    public final void addCar(Car car){
        cars.add(car);
    }

}
